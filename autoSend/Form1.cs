﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Data.Filtering;
using SOM5.Classes.DatabaseLibrary;
using SOM5.Classes.EMail;
using SOM5.Classes.Reports;

namespace som4Autosend
{
    public partial class Form1 : Form
    {
        emaillibrary emailer;
        string contactName = "";

        public Form1()
        {
            string connStr = MSSqlConnectionProvider.GetConnectionString("10.0.1.3", "sa", "Lov99tea", "SOM5");
            XpoDefault.DataLayer = XpoDefault.GetDataLayer(connStr, AutoCreateOption.SchemaAlreadyExists);
            InitializeComponent();
            emailer = new emaillibrary();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void sendCoRD(Loads load)
        {            
            contactName = "";
            load.Reload();            

            List<string> email = new List<string>();
            List<string> bcc = new List<string>();
            bcc.Add("administrator@sunnking.com");
            bcc.Add("nquilliam@sunnking.com"); //Temp in here to ensure CoRD's are being sent properly (7/13/2018)
            if (true)
            {                
                foreach (AccountContact contact in load.load_Account.account_Contact)
                {
                    if (contact.relationCertificateContact)
                        if (!contact.Contacts.contactEMailAddress.Equals(""))
                        {
                            email.Add(contact.Contacts.contactEMailAddress.Trim());
                            contactName = contactName + ", " + contact.Contacts.contactFirstName + " " + contact.Contacts.contactLastName;
                        }
                }
                if (email.Count == 0)
                {
                    foreach (AccountContact contact in load.load_Account.account_Contact)
                    {
                        if (contact.relationPrimaryContact)
                            if (!contact.Contacts.contactEMailAddress.Equals(""))
                            {
                                email.Add(contact.Contacts.contactEMailAddress.Trim());
                                contactName = contactName + ", " + contact.Contacts.contactFirstName + " " + contact.Contacts.contactLastName;
                            }
                    }
                    if (contactName.Length > 2)
                        contactName = contactName.Substring(2);
                }
                if (email.Count == 0)
                {                    
                    //There are no contacts elligible to recieve an email for the CoRD...notify Account owner
                    List<string> errorTo = new List<string>();
                    errorTo.Add(load.load_Account.account_Owner.email);                    
                    emailer.SendEMail("Missing account contact", "Load " + load.Oid + " needs to have it's certificate of recycling sent out. Please make sure " + load.load_Account.full_account_name + " has a primary or certificate contact, and that contact has an email address. Let Amy Spall know when you have fixed this, so the certificate can be sent out as soon as possible.<br /><br /> Thank you,<br /> Sunnking Ops team", errorTo, new List<string>(), bcc, new List<attachments>());
                }
            }
            // We have email addresses, main screen turn on....or something....
            // Try to set the CoRD flag first before actually emailing out

            
            //Just be totally sure the CoRD flag wasn't JUST set....

                load.Reload();
                if (load.CoRDsent != null)
                    return;
                load.CoRDsent = (DateTime)session1.Evaluate(typeof(XPObjectType), new FunctionOperator(FunctionOperatorType.Now), null);
                load.Save();

            attachments file = new attachments();
            rptCertOfRecyclingDestruction rpt = new rptCertOfRecyclingDestruction(load);
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            rpt.CreateDocument();
            rpt.ExportToPdf(stream);
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            file.FileContent = stream;
            file.FileName = "CoRD.pdf";

            System.IO.StreamReader myFile = new System.IO.StreamReader("body.txt");
            string message = myFile.ReadToEnd();
            myFile.Close();
            string body = "Dear " + load.load_Account.full_account_name + "<br /><br /> Attached is your Certificate of Recycling and Destruction for your Load " + load.Oid + @" that we picked up on " + ((DateTime)load.load_scheduleDate.Created).ToShortDateString() + ".<br /><br /> If you have any questions, please contact your Sales Rep " + load.load_Account.account_Owner.displayName + " at " + load.load_Account.account_Owner.workPhone + " or via email at <a href=\"mailto:" + load.load_Account.account_Owner.email + "\">" + load.load_Account.account_Owner.email + "</a>.<br /><br /> Thank You,<br /><br /> Sunnking Electronics Recycling";

            body = message.Replace("#body#", body);

            List<attachments> attachments = new List<attachments>();
            attachments.Add(file);
            emailer.SendEMail(load.load_Account.full_account_name + " Load " + load.Oid, body, email, new List<string>(), bcc, attachments);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!session1.IsConnected)
            {
                try
                {
                    session1.Disconnect();
                }
                catch { }
                try
                {
                    session1.Connect();
                }
                catch { }
            }

            session1.DropIdentityMap();
            xpCollection1.Reload();
            xpCollection2.Reload();
            gridControl1.BeginUpdate();
            gridControl1.RefreshDataSource();
            gridView1.RefreshData();
            gridControl1.Refresh();
            gridControl1.EndUpdate();

            foreach (Accounts act in xpCollection2)
            {
                if (((DateTime)act.accountExpire).Date.CompareTo(DateTime.Today.Date) <= 0)
                {
                    act.accountExpire = null;
                    act.activeAccount = false;
                    act.Save();
                }
            }

            DateTime day;      
            foreach (Loads load in xpCollection1)
            {
                load.Reload();
                if (!load.load_Account.autoSend || load.CoRDsent.HasValue)
                    continue;
                day = (DateTime)load.load_recievedFinish;
                //check to see if the load was finished more than 1 work day ago.
                if (day.DayOfWeek == DayOfWeek.Friday || day.DayOfWeek == DayOfWeek.Thursday)
                    day = day.AddDays(4);                
                else
                    day = day.AddDays(2);


                if (day.CompareTo(DateTime.Now) == -1)
                {
                    try
                    {
                        sendCoRD(load); 
                    }
                    catch (Exception ex)
                    {
                        load.Reload();
                        load.CoRDsent = null;
                        if (ex.Message.Length >= 100)
                            load.CoRDError = ex.Message.Substring(0, 99);
                        else
                            load.CoRDError = ex.Message;
                        load.Save();
                        //MessageBox.Show("Error with load " + load.Oid + "\n" + ex.Message);
                    }
                }
            }

            //now we will look for sales on the retail pos system to put into our ops manager...
            //checkPOS();
        }

        private void checkPOS()
        {
            string connStr = MSSqlConnectionProvider.GetConnectionString("10.5.1.11", "sa", "Lov99tea", "RETAILPOS");
            Session RetailSession = new Session() { ConnectionString = connStr };
            if (!RetailSession.IsConnected)
                RetailSession.Connect();
            string sqlSales = @"
SELECT [Transaction].TransactionNumber
		,ItemLookupCode
		,[TransactionEntry].price				
        ,[Transaction].[Time]
  FROM [RETAILPOS].[dbo].[TransactionEntry]
  inner join [RETAILPOS].[dbo].[Transaction] on TransactionEntry.TransactionNumber = [Transaction].TransactionNumber
  inner join [RETAILPOS].[dbo].[Item] on ItemID = item.ID  
  where ItemLookupCode like 'SKI%' and RecallType = 0
  order by [TransactionEntry].TransactionTime";                        
            var data = RetailSession.ExecuteQuery(sqlSales);
            foreach (var row in data.ResultSet[0].Rows)
            {                
                string itemLookupCode = row.Values[1].ToString();
                decimal? price = row.Values[2] as decimal?;                
                DateTime? time = row.Values[3] as DateTime?;
                
                int invID;
                Int32.TryParse(itemLookupCode.Substring(3), out invID);                
                    InventoryWares ware = session1.GetObjectByKey<InventoryWares>(invID, true);             
                    ware.soldPrice = Convert.ToDecimal(price.Value);                                                                       
                    ware.soldDate = time;
                    ware.Save();
                
                
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkPOS();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            timer1_Tick(null, null);
        }
    }    
}
